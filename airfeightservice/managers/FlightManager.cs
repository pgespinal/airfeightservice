﻿using System;
using System.Collections.Generic;
using airfeightservice.models;

namespace airfeightservice.managers
{
    public class FlightManager
    {

        public static Dictionary<int,List<Flight>> flights;
        private static int flightNo { get; set; }
        public static int day { get; set; }

        public FlightManager()
        {
            flights = new Dictionary<int, List<Flight>>();
        }

        public Dictionary<int,List<Flight>> Loadflight(int days) {
            for (int i = 0; i < days; i++)
            {
                flights.Add(++day, new List<Flight> { new Flight(++flightNo, "YUL", "YYZ", day),
                    new Flight(++flightNo, "YUL", "YYC", day),
                    new Flight(++flightNo, "YUL", "YVR", day)
            });
            }
           
            return flights;
        }

        public void PrintFlight() {

            foreach (var day in flights)
            {
                Console.WriteLine("Day: {0}", day.Key);
                foreach (var flight in day.Value)
                {
                    Console.WriteLine("Flight: {0} Departure: {1} Arrival: {2} Day: {3}", flight.flightNo, flight.departureCity, flight.arrivalCity, flight.day);
                }
                
            }
        }
    }
}
