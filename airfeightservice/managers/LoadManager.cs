﻿using System;
using System.Collections.Generic;
using System.IO;
using airfeightservice.models;
using Newtonsoft.Json;

namespace airfeightservice.managers
{
    public class LoadManager
    {
        List<Order> orders;

        public LoadManager()
        {
            orders = new List<Order>();
        }

        public List<Order> loadJsonfile() {
            using (StreamReader r = new StreamReader("../../resources/coding-assigment-orders.json"))
            {
                string json = r.ReadToEnd();
                var orderList = JsonConvert.DeserializeObject<Dictionary<string, Order>>(json);

                
                foreach (var item in orderList)
                {
                    orders.Add(new Order(item.Key, item.Value.destination));
                 }
            }

            return orders;
        }
    }
}
