﻿using System;
using System.Collections.Generic;
using airfeightservice.models;

namespace airfeightservice.managers
{
    public class PackageManager
    {

        private Queue<Order> orders;
        private Dictionary<int, List<Flight>> flights;
        private Queue<Order> orderNotScheduled;
        private FlightManager fm;
        private LoadManager lm;
        private const int DAY_FLIGHT = 2;
        private List<Flight> day1;
        private List<Flight> day2;

        public PackageManager()
        {
            orders = new Queue<Order>();
            orderNotScheduled = new Queue<Order>();
            fm = new FlightManager();
            lm = new LoadManager();
            day1 = new List<Flight>();
            day2 = new List<Flight>();
        }

        public void LoadOrderFlight() {
            lm.loadJsonfile().ForEach(o => orders.Enqueue(o));
            flights = fm.Loadflight(DAY_FLIGHT);

            while(orders.Count > 0)
            {
                Order order = orders.Peek();
                day1 = flights[1];
                day2 = flights[2];

                switch (order.destination)
                {
                    case "YYZ":
                        if (day1[0].orders.Count < 20)
                        {
                            day1[0].orders.Add(orders.Dequeue());
                        } else if(day2[0].orders.Count < 20){
                            day2[0].orders.Add(orders.Dequeue());
                        }
                        else
                        {
                            orderNotScheduled.Enqueue(orders.Dequeue());
                        }
                            break;
                    case "YYC":
                        if (day1[1].orders.Count < 20)
                        {
                            day1[1].orders.Add(orders.Dequeue());
                        }
                        else if (day2[1].orders.Count < 20)
                        {
                            day2[1].orders.Add(orders.Dequeue());
                        }
                        else
                        {
                            orderNotScheduled.Enqueue(orders.Dequeue());
                        }
                        break;
                    case "YVR":
                        if (day1[2].orders.Count < 20)
                        {
                            day1[2].orders.Add(orders.Dequeue());
                        }
                        else if (day2[2].orders.Count < 20)
                        {
                            day2[2].orders.Add(orders.Dequeue());
                        }
                        else
                        {
                            orderNotScheduled.Enqueue(orders.Dequeue());
                        }

                        break;


                    default:
                        orderNotScheduled.Enqueue(orders.Dequeue());
                        break;
                }

            }

            
        }

        public void PrintFlight()
        {
            foreach (var flight in day1)
            {
                flight.orders.ForEach(d => Console.WriteLine("Order: {0} FlightNumber: {1} Departure: {2} Arrival: {3} Day: {4}",
                        d.orderNo, flight.flightNo, flight.departureCity, flight.arrivalCity, flight.day));
            }

            foreach (var flight in day2)
            {
                flight.orders.ForEach(d => Console.WriteLine("Order: {0} FlightNumber: {1} Departure: {2} Arrival: {3} Day: {4}",
                        d.orderNo, flight.flightNo, flight.departureCity, flight.arrivalCity, flight.day));
            }

            foreach (var item in orderNotScheduled)
            {
                Console.WriteLine("order: {0} FlightNumber: not Scheduled", item.orderNo);
            }

        } 
    }
}
