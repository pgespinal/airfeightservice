﻿using System;
using airfeightservice.managers;

namespace airfeightservice
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            FlightManager fm = new FlightManager();
            PackageManager pm = new PackageManager();

            pm.LoadOrderFlight();

            fm.PrintFlight();
            pm.PrintFlight();
        }
    }
}
