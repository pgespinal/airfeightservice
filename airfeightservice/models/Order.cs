﻿using System;
namespace airfeightservice.models
{
    public class Order
    {
        public string orderNo { get; set; }
        public string destination { get; set; }
        
        public Order()
        {
        }

        public Order(string orderNo, string destination)
        {
            this.orderNo = orderNo;
            this.destination = destination;
        }
    }
}
