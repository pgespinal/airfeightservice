﻿using System;
using System.Collections.Generic;

namespace airfeightservice.models
{
    public class Flight
    {

        public int flightNo { get; set; }
        public string departureCity { get; set; }
        public string arrivalCity { get; set; }
        public int day { get; set; }
        public List<Order> orders { get; set; }


        public Flight()
        {
            orders = new List<Order>();
        }

        public Flight(int flightNo, string departureCity, string arrivalCity, int day)
        {
            this.flightNo = flightNo;
            this.departureCity = departureCity;
            this.arrivalCity = arrivalCity;
            this.day = day;
            orders = new List<Order>();
        }
    }
}
